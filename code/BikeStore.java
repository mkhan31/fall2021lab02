//Mohammad Khan, 2038700

public class BikeStore
{
    public static void main(String[] args)
    {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("Argon", 6, 30);
        bikes[1] = new Bicycle("Banshee", 7, 40);
        bikes[2] = new Bicycle("Circa", 8, 50);
        bikes[3] = new Bicycle("Evolve", 9, 60);
        for(Bicycle bike : bikes)
        {
            System.out.println(bike);
        }
    }
}