//Mohammad Khan, 2038700

public class Bicycle
{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int gears, double speed)
    {
        this.manufacturer = manufacturer;
        this.numberGears = gears;
        this.maxSpeed = speed;
    }

    public String getManufacturer()
    {
        return this.manufacturer;
    }

    public int getGear()
    {
        return this.numberGears;
    }

    public double getSpeed()
    {
        return this.maxSpeed;
    }

    public String toString()
    {
        return "Manufacturer: " + this.manufacturer + "\nNumber of gears: " + this.numberGears + "\nMax speed: " + this.maxSpeed + "\n";
    }
}